import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // System
    notifying: false,
    waitState: '',
    errmsg: '',
    flavorText: 'We got $n books you might interested!',
    flavorTexts: {
      random: 'We got $n books you might interested!',
      search: 'We found $n books..',
      filter: ' ($m were filtered by some of your filters)'
    },
    // Book
    currentBooks: [],
    favBookIds: [],
    lastQuery: {},
    currentIndex: 0,
    maxPerPage: 40,
    startIndex: 0,
    fields: 'kind,totalItems,items(id,volumeInfo/title,volumeInfo/subtitle,volumeInfo/authors,volumeInfo/publishedDate,volumeInfo/description,volumeInfo/pageCount,volumeInfo/industryIdentifiers,volumeInfo/categories,volumeInfo/imageLinks/thumbnail,volumeInfo/infoLink,volumeInfo/language,saleInfo/isEbook)'

  },
  mutations: {
    CLOSE_NOTIFY: state => {
      state.notifying = false
    },
    SUCCESS_NOTIFY: state => {
      state.notifying = true
      state.waitState = 'success'
      setTimeout(() => (state.notifying = false), 1000)
    },
    ERROR_NOTIFY: (state, payload) => {
      state.notifying = true
      state.waitState = 'fail'
      state.errmsg = payload
    },
    SET_CURRENT_BOOKS: (state, payload) => {
      state.currentBooks = payload
    },
    ADD_FAV_BOOK: (state, payload) => {
      state.favBookIds.push(payload)
      localStorage.setItem('favs', JSON.stringify(state.favBookIds))
    },
    REMOVE_FAV_BOOK: (state, payload) => {
      state.favBookIds.splice(state.favBookIds.indexOf(payload),1)
      localStorage.setItem('favs',JSON.stringify(state.favBookIds))
    },
    LOAD_FAV_BOOK: (state) => {
      let favCookie = localStorage.getItem('favs')
      if (favCookie)
        state.favBookIds = JSON.parse(favCookie)
      else
        state.favBookIds = []
    },
    SET_LAST_QUERY: (state,payload) => {
      state.lastQuery = payload
    }
  },
  actions: {
    async search ({
      commit,state,dispatch
    },query) {
      try {
        // Check mode of query
        // MODE random: Populate random books (based on user preference if possible)
        if (query.mode === 'random') {
          if (state.favBookIds.length > 0) {
            /* Random recommended books */
            // Get one in fav
            let url = `${process.env.VUE_APP_API_URL_GOOGLE_BOOK}/volumes/${global._.sample(state.favBookIds)}`
            let result = await global.axios.get(url)
            query.text = global._.sample([
              `subject:${global._.sample(result.data.volumeInfo.categories||'')}`,
              `inauthor:${global._.sample(result.data.volumeInfo.authors ||[])}`,
            ])
          } else {
            /* Random any books */
            query.text = String.fromCharCode(Math.floor((Math.random() * 25) + 97))
          }
          state.currentIndex = 0
          state.flavorText = state.flavorTexts['random']
        }
        // MODE next: if this query is continuous from last query
        else if (query.mode === 'next') {
          query = { ...state.lastQuery, ...query }
          state.currentIndex += state.maxPerPage
        }
        else {
          state.currentIndex = 0
          state.flavorText = state.flavorTexts['search']
        }

        // Fix query
        if (query.text) {
          if (query.text.indexOf(' ') > -1)
            query.text = query.text.split(' ').join('+')
        }
        console.log(query)
        // QUERY
        let url = `${process.env.VUE_APP_API_URL_GOOGLE_BOOK}/volumes/?`+
          `q=${query.text || '""'}`+
          `${query.author ? ('+inauthor:' + query.author):''}&` +
          // User filters
          `printType=${query.type || 'all'}&` +
          `maxAllowedMaturityRating=${query.mature ? 'mature':'not-mature'}&` +
          ((query.isEbook === 'ebooks') ? `filter=${(query.free) ? 'free-ebooks' : 'ebooks'}` : '') +

          `orderBy=${query.orderBy || 'relevance'}&` +
          `startIndex=${state.currentIndex}&` +
          `maxResults=${state.maxPerPage}&` +
          `fields=${state.fields}&`
        console.log(url)
        // Result
        let result = await global.axios.get(url)
        let data = result.data
        if ('error' in data) throw data.error
        // Check if no book left
        if (!('items' in data)) {
          // If random got none, random new one
          if (query.mode === 'random') {
            dispatch('search', { mode: 'search',text: 'a' })
            return 'complete'
          }
          // If other mode (and not 'next') got none, just set books to none
          else if (query.mode === 'search') {
            commit('SET_CURRENT_BOOKS', data)
          }
          return 'complete'
        }
        // Filter duplicates
        let uniqueId = [...new Set(data.items.map(x=>x.id))]
        data.items = uniqueId.map(uid => data.items.find(item => item.id === uid))

        // Check if set new one or add results
        if (query.mode === 'next') {
          commit('SET_CURRENT_BOOKS', { ...data,items: [...state.currentBooks.items,...data.items]})
        } else if (query.mode === 'search') {
          // Filter manually (Can't use API)
          let beforeFilterCount = data.items.length
          if (query.isEbook === 'non') {
            data.items = data.items.filter(x => !x.saleInfo.isEbook)
          }
          if (query.language) {
            data.items = data.items.filter(x => x.volumeInfo.language === query.language)
          }

          // Update flavorText if some got manually filtered
          if (data.items.length < beforeFilterCount) {
            state.flavorText = state.flavorText + state.flavorTexts['filter'].replace('$m', beforeFilterCount - data.items.length)
          }

          commit('SET_CURRENT_BOOKS', data)
        } else {
          commit('SET_CURRENT_BOOKS', data)
        }

        commit('SET_LAST_QUERY', query)
        return 'loaded'
      } catch (e) {
        console.error(e)
        commit('ERROR_NOTIFY', e)
        return 'error'
      }
    },
    async getBookDetail({
      commit, state
    }, id) {
      try {
        let url = `${process.env.VUE_APP_API_URL_GOOGLE_BOOK}/volumes/${id}`
        let result = await global.axios.get(url)
        return result.data
      } catch(e) {
        console.error(e)
        return {'error':e}
      }
    }
  },
  getters: {
    isFav: state => book => state.favBookIds.indexOf(book.id) > -1
  }
})
