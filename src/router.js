import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/PageHome.vue')
    },
    {
      path: '/book/:id',
      name: 'book',
      component: () => import('./views/PageDetail.vue')
    },
    {
      path: '/fav',
      name: 'favourite',
      component: () => import('./views/PageFav.vue')
    }
  ]
})
